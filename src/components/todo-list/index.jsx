import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodoAction } from "../../redux/actions/todo";
import TodoItem from "../todo-item";

export default function TodoList() {
  const [isNewTodo, setNewTodo] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const todos = useSelector((state) => state.todos.todos);
  const dispatch = useDispatch();

  function addTodo() {
    dispatch(addTodoAction(inputValue));
    setNewTodo(false);
    setInputValue("");
  }

  return (
    <div>
      <h2>My awesome todo list</h2>
      {todos &&
        todos.map((todo) => <TodoItem id={todo.idx} key={todo.idx} text={todo.text} status={todo.status}/>)}
      {isNewTodo ? (
        <div className="add-new-todo">
          <input
            placeholder="insert your todo"
            onInput={(e) => setInputValue(e.target.value)}
          ></input>
          <button onClick={() => (inputValue ? addTodo() : null)}>add</button>
        </div>
      ) : (
        <button onClick={() => setNewTodo(true)} className="new-todo">
          new todo
        </button>
      )}
    </div>
  );
}
