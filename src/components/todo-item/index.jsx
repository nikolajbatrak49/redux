
import { useDispatch } from "react-redux";
import {
  deleteTodoAction,
  changeStatusTodo,
} from "../../redux/actions/todo";

export default function TodoItem(props) {

  const dispatch = useDispatch();

  function changeStatus() {
    const newStatus = props.status === 'done' ? 'todo': 'done';
    dispatch(changeStatusTodo(props.id, newStatus));
  }

  return (
    <div className="todo-item">
      <span
        style={{
          textDecoration: props.status === "done" ? "line-through" : "none",
        }}
      >
        {props.text}
      </span>
      <div className="buttons">
        <button
          onClick={changeStatus}
        >
          mark as {props.status === "done" ? "undone" : "done"}
        </button>
        <button onClick={()=>{dispatch(deleteTodoAction(props.id))}}>remove from list</button>
      </div>
    </div>
  );
}
