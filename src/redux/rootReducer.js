// Core
import { combineReducers } from 'redux';

// Reducers

import { todoReducer as todos } from './reducers/todo';

export const rootReducer = combineReducers({
    todos
});
