import { todoTypes } from "../types";

export function addTodoAction(todo) {
  return {
    type: todoTypes.ADD,
    payload: { idx: Date.now(), text: todo, status: "todo" },
  };
}

export function deleteTodoAction(id) {
  return {
    type: todoTypes.DELETE,
    payload: {id},
  };
}

export function changeStatusTodo(id, status) {
  return {
    type: todoTypes.CHANGE_STATUS,
    payload: {id, status},
  };
}
