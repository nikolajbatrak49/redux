import { todoTypes } from "../types";

const initialState = {
  todos: [],
};

function getChangedTodoIdx(state, id) {
  return state.todos.findIndex((todo) => todo.idx === id)
}

export function todoReducer(state = initialState, action) {
    
  switch (action.type) {
    case todoTypes.ADD:
      return {
        todos: [...state.todos, action.payload],
      };

    case todoTypes.DELETE:
      const todoToDeleteIdx = getChangedTodoIdx(state, action.payload.id);
      if (!~todoToDeleteIdx) return state;
      return {
        todos: [
          ...state.todos.slice(0, todoToDeleteIdx),
          ...state.todos.slice(todoToDeleteIdx + 1),
        ],
      };
      
    case todoTypes.CHANGE_STATUS:
      const changedTodoIdx = getChangedTodoIdx(state, action.payload.id);
    
      if (!~changedTodoIdx) return state;
      return {
        todos: [
          ...state.todos.slice(0, changedTodoIdx),
          { ...state.todos[changedTodoIdx], status: action.payload.status},
          ...state.todos.slice(changedTodoIdx + 1),
        ],
      };
    
    default:
      return state;
  }
}
